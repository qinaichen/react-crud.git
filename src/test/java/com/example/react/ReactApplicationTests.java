package com.example.react;

import com.example.react.model.User;
import com.example.react.model.UserDao;
import lombok.AllArgsConstructor;
import org.apache.commons.text.RandomStringGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.stream.IntStream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ReactApplicationTests {

	@Autowired
	private UserDao userDao;

	@Test
	public void contextLoads() {

        RandomStringGenerator generator = new RandomStringGenerator.Builder().withinRange('a','z').build();

        IntStream.rangeClosed(1,10).forEach(item->{
            String name = generator.generate(8);
            this.userDao.save(new User().setName(name));
        });

	}

}
