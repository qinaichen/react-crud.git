package com.example.react.controller;

import com.example.react.model.User;
import com.example.react.model.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserDao userDao;

    @GetMapping
    public List<User> all(){
        return this.userDao.findAll();
    }

    @PostMapping
    public Object save(@RequestBody User user){
        this.userDao.save(user);
        return true;
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable Long id){
        this.userDao.deleteById(id);
        return true;
    }
}
